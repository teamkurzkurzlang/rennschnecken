package Rennschnecken;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;



public class Main {
    void winner(int position) {
        int start = 0;
        int ziel = 1000;

    }
    public static void main(String[] args) {
        /** Interface
        System.out.println("Username: ");
        Scanner input = new Scanner(System.in);
        String username = input.next();
        System.out.println("Schnecken Name: ");
        String schneckenName = input.next();
        System.out.println("Anzahl Gegner (Max 5): ");
        String anzahl = input.next();
        input.close();
        **/
        String username1 = "Digit";
        String schneckenName = "Ruby";
        int anzahl = 4;  // Anzahl Schnecken
        anzahl = anzahl + 1;
        int maxDistanceUser = 0;
        int maxDistanceNPCs = 0;
        int distanceNPCs[] = new int[6];
        int distanceUser[] = new int[1];
        int ii = 0;
        int leaderNPCs = 0;
        int leaderUser = 0;

        Rennen r1 = new Rennen();
        User1 user1 = new User1(username1);

        // init distance array Position 0 is User, rest NPCs
        for (int i = 0; i<distanceNPCs.length; i++) {
            distanceNPCs[i] = 0;
        }
        for (int i = 0; i<distanceUser.length; i++) {
            distanceUser[i] = 0;
        }

        int maxNPCs = distanceNPCs[0];
        int maxUser = distanceUser[0];

        //ArrayLists for Users and NPCs
        ArrayList<NPCs> alNPCs = new ArrayList <>();
        ArrayList<User1> alUsers = new ArrayList <>();

        //add Users
        alUsers.add(0, new User1(username1));
        //Show Stats
        //System.out.println(alUsers.get(0).toString());
        //System.out.println(user1.toString());


        //add NPCs
        alNPCs.add(0, new NPCs());
        alNPCs.add(1, new NPCs());
        alNPCs.add(2, new NPCs());
        alNPCs.add(3, new NPCs());
        alNPCs.get(0).loadNPC(0);
        alNPCs.get(1).loadNPC(1);
        alNPCs.get(2).loadNPC(2);
        alNPCs.get(3).loadNPC(3);

        do {

            //calc Users add distance from random newstep()
            for (int i=0; i<distanceUser.length; i++) {
                int temp = r1.newstep();
                alUsers.get(0).setInts(temp, 0);
                //fill and check distance Array
                distanceUser[i] = distanceUser[i] + temp;

            }
            // calc NPCs
            for (int i=0; i<alNPCs.size(); i++) {
                int temp = r1.newstep();
                alNPCs.get(i).setInts(temp, 0);
                //fill and check distance Array
                distanceNPCs[i] = distanceNPCs[i] + temp;
                //find Max Distance in array
            }
            // get max Value out of distance array
            for (int y=0; y<distanceNPCs.length; y++) {
                if (distanceNPCs[y] > maxDistanceNPCs) {
                    maxDistanceNPCs = distanceNPCs[y];
                    leaderNPCs = y;
                }
            }
            System.out.println("Stand nach Runde: " + ii + " " + alUsers.get(0).toString());
            System.out.println("Stand nach Runde: " + ii + " " + alNPCs.get(0).toString(0));
            System.out.println("Stand nach Runde: " + ii + " " + alNPCs.get(1).toString(1));
            System.out.println("Stand nach Runde: " + ii + " " + alNPCs.get(2).toString(2));
            System.out.println("Stand nach Runde: " + ii + " " + alNPCs.get(3).toString(3));

            ii++;
        } while(maxDistanceNPCs<100);
        System.out.println("Gewinner: " + alNPCs.get(leaderNPCs).toString(leaderNPCs));
        //System.out.println(alNPCs.get(0).toString(0));
        //alNPCs.get(0).setInts(10,10);
        //alNPCs.get(0).loadNPC(0);
        //System.out.println(alNPCs.get(0).toString(0));
        //alNPCs.get(0).setInts(15,10);


    }
}
